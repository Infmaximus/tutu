package com.infmaximus.tututest.app;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.infmaximus.tututest.di.AppComponent;
import com.infmaximus.tututest.di.AppModule;
import com.infmaximus.tututest.di.DaggerAppComponent;
import com.infmaximus.tututest.di.PresenterModule;

/**
 * Created by InfMaximus on 16.11.2017.
 */

public class App extends Application {

    private static AppComponent appComponent;
    public static AppComponent getAppComponent(){
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        appComponent = DaggerAppComponent.builder()
                .presenterModule(new PresenterModule())
                .appModule(new AppModule(this))
                .build();

    }
}

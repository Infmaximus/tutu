package com.infmaximus.tututest.di;

import com.infmaximus.tututest.model.NewsModel;
import com.infmaximus.tututest.presenter.mainnews.NewsPresenter;
import com.infmaximus.tututest.view.mainnews.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by InfMaximus on 16.11.2017.
 */

@Component(modules = {PresenterModule.class,ModelModule.class,NewsApiModule.class,AppModule.class})
@Singleton
public interface AppComponent {
    void inject(MainActivity mainActivity);
    void inject(NewsPresenter newsPresenter);
    void inject(NewsModel newsModel);
}

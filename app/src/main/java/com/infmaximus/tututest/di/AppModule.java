package com.infmaximus.tututest.di;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by InfMaximus on 16.11.2017.
 */
@Module
public class AppModule {

    private Context mContext;

    public AppModule(Context aContext){
        mContext = aContext;
    }

    @Provides
    @NonNull
    @Singleton
    public Context getAppContext(){
        return mContext;
    }

}

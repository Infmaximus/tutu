package com.infmaximus.tututest.di;

import android.support.annotation.NonNull;

import com.infmaximus.tututest.model.BaseNewsModel;
import com.infmaximus.tututest.model.NewsModel;
import com.infmaximus.tututest.model.newsapi.BaseNewsApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by InfMaximus on 16.11.2017.
 */

@Module
public class ModelModule {

    @Provides
    @NonNull
    @Singleton
    public BaseNewsModel getNewsModel(BaseNewsApi aNewsApi){
        return new NewsModel(aNewsApi);
    }

}

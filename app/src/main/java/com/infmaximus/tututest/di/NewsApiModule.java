package com.infmaximus.tututest.di;

import android.support.annotation.NonNull;

import com.infmaximus.tututest.model.newsapi.BaseNewsApi;
import com.infmaximus.tututest.model.newsapi.NYTimesNewsApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by InfMaximus on 16.11.2017.
 */

@Module
public class NewsApiModule {

    @Provides
    @NonNull
    @Singleton
    public BaseNewsApi provideNewsApi(){
        return new NYTimesNewsApi();
    }

}

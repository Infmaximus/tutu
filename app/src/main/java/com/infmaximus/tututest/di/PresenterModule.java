package com.infmaximus.tututest.di;

import android.support.annotation.NonNull;

import com.infmaximus.tututest.presenter.adapter.BaseListPresenter;
import com.infmaximus.tututest.presenter.adapter.ListPresenter;
import com.infmaximus.tututest.model.BaseNewsModel;
import com.infmaximus.tututest.presenter.mainnews.BaseNewsPresenter;
import com.infmaximus.tututest.presenter.mainnews.NewsPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by InfMaximus on 16.11.2017.
 */

@Module
public class PresenterModule {

    @Provides
    @NonNull
    @Singleton
    public BaseNewsPresenter provideNewsPresenter(BaseNewsModel aNewsModel,BaseListPresenter aBaseListPresenter){
        return new NewsPresenter(aNewsModel,aBaseListPresenter);
    }

    @Provides
    @NonNull
    @Singleton
    public BaseListPresenter provideListPresenter(BaseNewsModel aNewsModel){
        return new ListPresenter(aNewsModel);
    }

}

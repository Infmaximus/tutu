package com.infmaximus.tututest.model;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by InfMaximus on 16.11.2017.
 */

public interface BaseNewsModel {

    //Запрос на загрузку первой страницы/части новостей
    void loadNews();
    //Запрос на получение адреса этой новости
    void openNews(int i);
    //Скрыть новость
    void closeNews();
    //Запрос на загрузку новой (следующей по индексу) страницы/части новостей
    void loadNewPage();
    //Получение списка всех новостей для дальнейшего отображения
    BehaviorSubject<List> getLastNewsListSubject();
    //Получение адреса последней запрошеной новости
    BehaviorSubject<String> getLastNewsSubject();
    //Получение информации о текущей загрузке данных
    BehaviorSubject<Boolean> getStateLoadSubject();
    //Получение текста ошибки
    PublishSubject<String> getErrorCodeSubject();
}

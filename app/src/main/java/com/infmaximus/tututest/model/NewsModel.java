package com.infmaximus.tututest.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.infmaximus.tututest.R;
import com.infmaximus.tututest.app.App;
import com.infmaximus.tututest.model.newsapi.BaseNewsApi;
import com.infmaximus.tututest.objects.News;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

/**
 * Общая модель приложения
 */

public class NewsModel implements BaseNewsModel{

    @Inject
    Context mContext;

    private final int ERROR_NO_CONNECTIONS = 1;
    private final int ERROR_NO_AUTHORIZATION = 2;
    private final int ERROR_UNKNOWN_ERROR = 3;

    private BehaviorSubject<List> mLastNewsListSubject = BehaviorSubject.create();
    private BehaviorSubject<String> mLastNewsSubject = BehaviorSubject.create();
    private BehaviorSubject<Boolean> mStateLoadSubject = BehaviorSubject.create();
    private PublishSubject<String> mErrorCodeSubject = PublishSubject.create();

    private final boolean START_LOAD = true;
    private final boolean END_LOAD = false;
    private final String EMPTY_URI = "";
    private final int FIRST_PART = 1;
    private List<News> mNewsNews;
    //Группа новостей по номеру, в частном случае номер страницы
    private int mPartNews = FIRST_PART;

    private BaseNewsApi mNewsApi;
    public NewsModel(BaseNewsApi aNewsApi) {

        App.getAppComponent().inject(this);

        mNewsApi = aNewsApi;
        //Начинаем всегда с 1 страницы
        mNewsApi.getErrorCodeSubject()
                .subscribe(errorCode-> {
                    switch (errorCode){
                        case ERROR_NO_CONNECTIONS:
                            mErrorCodeSubject.onNext(mContext.getString(R.string.no_connected));
                            break;
                        case ERROR_NO_AUTHORIZATION:
                            mErrorCodeSubject.onNext(mContext.getString(R.string.no_authorization));
                            break;
                        case ERROR_UNKNOWN_ERROR:
                            mErrorCodeSubject.onNext(mContext.getString(R.string.unknown_error));
                            break;
                    }
                });

        mNewsApi.getListNewsListSubject()
                .subscribe(aList->{
                    mStateLoadSubject.onNext(END_LOAD);
                    if(aList!=null) {
                        //инкрементируем страницу загрузки по удачной загрузке
                        incrementPathNews();
                        //Если список не был пуст, добавляем вновь загруженные статьи
                        if(mNewsNews==null)
                            mNewsNews = aList;
                        else
                            mNewsNews.addAll(aList);
                        mLastNewsListSubject.onNext(mNewsNews);
                    }
                });

    }

    @Override
    public void loadNews() {
        try {
            if(isOnline()) {
                //Делаем запрос на загрузку/перезагрузку первой страницы
                mStateLoadSubject.onNext(START_LOAD);
                mPartNews = FIRST_PART;
                mNewsApi.requestListNews(mPartNews);
                mNewsNews.clear();
            } else {
                mErrorCodeSubject.onNext(mContext.getString(R.string.no_connected));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void openNews(int i) {
        if(mNewsNews.size()>i)
            mLastNewsSubject.onNext(mNewsNews.get(i).getUrl());
    }

    @Override
    public void closeNews() {
        //Пустая строка интерпретируется, как скрытие последней новости
        mLastNewsSubject.onNext(EMPTY_URI);
    }

    @Override
    public void loadNewPage() {
        try {
            if(isOnline()) {
                mStateLoadSubject.onNext(START_LOAD);
                mNewsApi.requestListNews(mPartNews);
            }else {
                mErrorCodeSubject.onNext(mContext.getString(R.string.no_connected));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public BehaviorSubject<List> getLastNewsListSubject() {
        return mLastNewsListSubject;
    }

    @Override
    public BehaviorSubject<String> getLastNewsSubject() {
        return mLastNewsSubject;
    }

    @Override
    public BehaviorSubject<Boolean> getStateLoadSubject() {
        return mStateLoadSubject;
    }

    @Override
    public PublishSubject<String> getErrorCodeSubject() {
        return mErrorCodeSubject;
    }

    //Инкрементация индекса для загрузки следующей страницы
    private void incrementPathNews(){
        mPartNews++;
    }

    //Проверка на подключение к сети
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}

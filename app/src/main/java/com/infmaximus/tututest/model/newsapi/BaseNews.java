package com.infmaximus.tututest.model.newsapi;

/**
 * Гарант получения нужных полей после парсинга
 */

public interface BaseNews {

    String getShortNews();
    String getImageUri();
    String getWebSite();
    String getTime();
    String getLargeNews();

}

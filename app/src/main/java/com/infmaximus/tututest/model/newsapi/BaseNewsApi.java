package com.infmaximus.tututest.model.newsapi;

import com.infmaximus.tututest.objects.News;

import java.util.List;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by InfMaximus on 16.11.2017.
 */

public interface BaseNewsApi {

    //Выполнить запрос на обновление статей
    void requestListNews(int aPage);
    //Получить список статей
    PublishSubject<List<News>> getListNewsListSubject();
    //Получить код ошибки
    PublishSubject<Integer> getErrorCodeSubject();

}

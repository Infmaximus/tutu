package com.infmaximus.tututest.model.newsapi.NYTimesApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("docs")
    @Expose
    private List<NYTimesNews> docs = null;
    @SerializedName("meta")
    @Expose
    private Meta meta;

    public List<NYTimesNews> getDocs() {
        return docs;
    }

    public void setDocs(List<NYTimesNews> docs) {
        this.docs = docs;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
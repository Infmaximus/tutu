package com.infmaximus.tututest.model.newsapi;

import android.support.annotation.NonNull;
import android.util.Log;

import com.annimon.stream.Stream;
import com.infmaximus.tututest.model.newsapi.NYTimesApi.JsonAnswer;
import com.infmaximus.tututest.objects.News;
import com.infmaximus.tututest.model.retrofit2.NewsLoader;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by InfMaximus on 16.11.2017.
 */

public class NYTimesNewsApi implements BaseNewsApi{

    //Можно и передать по инициализации модели
    private final String NYTIMES_KEY = "8ff1e2566e9c46a68c9368c9570727e0";
    private final int ERROR_NO_CONNECTIONS = 1;
    private final int ERROR_UNAUTORIZATION = 2;
    private final int ERROR_UNKNOWN_ERROR = 3;
    private Retrofit mRetrofit;
    private NewsLoader mNewsLoader;
    private PublishSubject<List<News>> getNewsListSubject = PublishSubject.create();
    private PublishSubject<Integer> getErrorCodeSubject = PublishSubject.create();
    private PublishSubject<Integer> getRequestPageSubject = PublishSubject.create();
    public NYTimesNewsApi(){
        mRetrofit = new Retrofit.Builder()
                //Базовая часть адреса
                .baseUrl("https://api.nytimes.com")
                //Преобразование
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mNewsLoader = mRetrofit.create(NewsLoader.class);

        //Основной механизм загрузки новостей TNYT
        getRequestPageSubject
                .observeOn(Schedulers.newThread())
                .flatMap(new Function<Integer, ObservableSource<List<? extends BaseNews>>>() {
                    @Override
                    public ObservableSource<List<? extends BaseNews>> apply(Integer integer) throws Exception {
                        return Observable.create(e1-> mNewsLoader.getNYTNewsJson(integer,NYTIMES_KEY).enqueue(new Callback<JsonAnswer>() {
                            @Override
                            public void onResponse(Call<JsonAnswer> call, retrofit2.Response<JsonAnswer> response) {
                                //Если после запроса body не пусто, продолжаем цепочку преобразований
                                if(response!=null&&response.code()==403)
                                    getErrorCodeSubject.onNext(ERROR_UNAUTORIZATION);
                                //Если ошибки отсутствуют и тело ответа не пусто используем данные
                                else if(response!=null&&response.body()!=null)
                                    e1.onNext(response.body().getResponse().getDocs());
                            }
                            @Override
                            public void onFailure(Call<JsonAnswer> call, Throwable t) {
                                //Произошла ошибка, фиксируем код (По хорошему стоит проанализировать)
                                getErrorCodeSubject.onNext(ERROR_UNKNOWN_ERROR);
                            }
                        }));
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .map(this::convertInfoToNews)
                .subscribe(aList-> getNewsListSubject.onNext(aList));
    }

    //Запрашиваем список новостей
    @Override
    public void requestListNews(int aPage) {
        try {
            getRequestPageSubject.onNext(aPage);
        }catch (Exception ex){
            Log.e("LOG_TAG", "ошибка загрузки новостей" + ex.toString());
            getErrorCodeSubject.onNext(ERROR_UNKNOWN_ERROR);
        }
    }

    @Override
    public PublishSubject<List<News>> getListNewsListSubject() {
        return getNewsListSubject;
    }

    @Override
    public PublishSubject<Integer> getErrorCodeSubject() {
        return getErrorCodeSubject;
    }

    //Преобразование новостей к необходимому виду
    public List<News> convertInfoToNews(@NonNull List<? extends BaseNews> aBaseNews){
        return Stream.of(aBaseNews)
                .map(bn-> new News.Builder()
                        .setDate(convertDate(bn.getTime()))
                        .setLongText("")
                        .setShortText(bn.getShortNews())
                        .setUrl(bn.getWebSite())
                        .setImageUri(convertImage(bn.getImageUri()))
                        .build())
                .toList();
    }

    //Преобразование времени к более простому виду
    public String convertDate(String aDate){
        return aDate.substring(0,aDate.indexOf('T'));
    }

    //Преобразование изображения
    private String convertImage(String aImageUri){
        return "https://static01.nyt.com/"+aImageUri;
    }
}

package com.infmaximus.tututest.model.retrofit2;

/**
 * Основной запрос для сайта TNYT
 */

import com.infmaximus.tututest.model.newsapi.NYTimesApi.JsonAnswer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface NewsLoader {

    @GET("/svc/search/v2/articlesearch.json")
    Call<JsonAnswer> getNYTNewsJson(@Query("page") int aPageNumber, @Header("api-key") String aApiKey);

}

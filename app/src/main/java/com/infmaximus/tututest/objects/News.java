package com.infmaximus.tututest.objects;

/**
 * Обхект с которым работает при отображении
 */

public class News {

    //Краткйи текст
    private String mShortText;
    //Полный текст новости
    private String mLongText;
    private String mUrl;
    private String mImageUri;
    private String mDate;

    public News(){

    }

    public String getShortText() {
        return mShortText;
    }

    public String getLongText() {
        return mLongText;
    }

    public String getImageUri() {
        return mImageUri;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getDate() {
        return mDate;
    }

    public static class Builder{

        private String mShortText;
        private String mLongText;
        private String mUrl;
        private String mDate;
        private String mImageUri;

        public Builder(){

        }

        public Builder setShortText(String aShortText){
            mShortText = aShortText;
            return this;
        }

        public Builder setLongText(String aLongText) {
            mLongText = aLongText;
            return this;
        }

        public Builder setUrl(String aUrl) {
            mUrl = aUrl;
            return this;
        }

        public Builder setDate(String aDate) {
            mDate = aDate;
            return this;
        }

        public Builder setImageUri(String aImageUri) {
            mImageUri = aImageUri;
            return this;
        }

        public News build(){
            News news = new News();
            news.mShortText = mShortText;
            news.mLongText = mLongText;
            news.mDate = mDate;
            news.mUrl = mUrl;
            news.mImageUri = mImageUri;

            return news;
        }

    }

}

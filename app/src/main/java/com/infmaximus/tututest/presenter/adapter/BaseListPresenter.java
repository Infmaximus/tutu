package com.infmaximus.tututest.presenter.adapter;

import com.infmaximus.tututest.view.adapter.BaseAdapterList;

/**
 * Интерфейс презентера для адаптера
 */

public interface BaseListPresenter {

    //Привязать адаптер
    void bindAdapter(BaseAdapterList aBaseAdapterList);
    //отвязать адаптер
    void unBind();

}

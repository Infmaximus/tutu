package com.infmaximus.tututest.presenter.adapter;

import com.infmaximus.tututest.view.adapter.BaseAdapterList;
import com.infmaximus.tututest.model.BaseNewsModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Реализация презентера для работы с адаптером
 */

public class ListPresenter implements BaseListPresenter{

    private CompositeDisposable mCompositDisposable;
    private BaseNewsModel mNewsModel;
    private BaseAdapterList mAdapterList;
    public ListPresenter(BaseNewsModel aNewsModel) {
        mNewsModel = aNewsModel;
    }

    @Override
    public void bindAdapter(BaseAdapterList aBaseAdapterList) {
        mAdapterList = aBaseAdapterList;
        mCompositDisposable = new CompositeDisposable();

        //Подписываемся на получение списка новостей, и отображаем их
        mCompositDisposable.add(
            mNewsModel.getLastNewsListSubject()
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(arrayList -> mAdapterList.setArrayList(arrayList))
                    .subscribe(arrayList -> mAdapterList.notifyDataSetChanged()));

        //Инициируем запрос на отображение конкретной новости по нажатию на позицию в списке
        mCompositDisposable.add(
            mAdapterList.getUriSubject()
                    .subscribe(aPos->mNewsModel.openNews(aPos)));

    }

    @Override
    public void unBind() {
        mAdapterList = null;
        if(mCompositDisposable!=null)
            mCompositDisposable.dispose();
    }
}

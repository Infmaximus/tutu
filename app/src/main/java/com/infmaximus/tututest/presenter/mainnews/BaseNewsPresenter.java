package com.infmaximus.tututest.presenter.mainnews;

import com.infmaximus.tututest.view.mainnews.BaseNewsView;

/**
 * Интерфейс презентера для работы с главным экраном (источником отображения всех новостей) приложения
 */

public interface BaseNewsPresenter {

    //Приатачиться к экрану
    void bindView(BaseNewsView aNewsView);

    //Отвязать экран
    void unBindView();


}

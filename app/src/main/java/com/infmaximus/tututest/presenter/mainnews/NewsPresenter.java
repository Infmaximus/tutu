package com.infmaximus.tututest.presenter.mainnews;

import com.infmaximus.tututest.presenter.adapter.BaseListPresenter;
import com.infmaximus.tututest.app.App;
import com.infmaximus.tututest.model.BaseNewsModel;
import com.infmaximus.tututest.view.mainnews.BaseNewsView;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Реализация презентера главного экрана
 */

public class NewsPresenter implements BaseNewsPresenter {

    private BaseNewsView mNewsView;
    private BaseNewsModel mNewsModel;
    private BaseListPresenter mListPresenter;
    //Для общего dispose используем CompositeDisposable
    private CompositeDisposable mCompositDisposable;

    public NewsPresenter(BaseNewsModel aNewsModel,BaseListPresenter aListPresenter){
        mNewsModel = aNewsModel;
        mListPresenter = aListPresenter;
        App.getAppComponent().inject(this);
    }

    //Приатачить View
    @Override
    public void bindView(BaseNewsView aNewsView) {
        mNewsView = aNewsView;

        //На всякий случай все затираем (Если Destroy не очень)
        if(mCompositDisposable!=null&&!mCompositDisposable.isDisposed())
            mCompositDisposable.dispose();

        mCompositDisposable = new CompositeDisposable();
        //mSubscriptionBtnLoad = mNewsView.getPSBtnUpdateClick().subscribe(b->());

        //Запрос загрузки новостей 1 странциы по нажатию кнопку с отображением диалога
        mCompositDisposable.add(mNewsView.getPSBtnUpdateClick()
                .doOnNext(aBoolean->mNewsView.showProgress(true))
                .observeOn(Schedulers.newThread())
                .subscribe(aBoolean -> mNewsModel.loadNews()));

/*        mSubscriptionLastNews = mNewsModel.getLastNewsListSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(arrayList -> mNewsView.showMessage("htpekmnfn"));
                mCompositDisposable.add(mSubscriptionLastNews);*/


        //Приатачивание адаптера к собственному презентеру
        mCompositDisposable.add(mNewsView.getAdapter()
                .subscribe(aRecycler->mListPresenter.bindAdapter(aRecycler)));

        //Отображе/скрытие диалога отображения новости по адресу новости
        mCompositDisposable.add(mNewsModel.getLastNewsSubject()
                .subscribe(aUri->mNewsView.initializationDlg(aUri)));

        //Подзагрузка новых новостей
        mCompositDisposable.add(mNewsView.getRecyclerScrollEnd()
                //Фильтруем множественные запросы
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.newThread())
                .subscribe(aBoolean -> mNewsModel.loadNewPage()));

        //Запрос наотображение/скрытие прогрессбара загрузки
        mCompositDisposable.add(mNewsModel.getStateLoadSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> mNewsView.showProgress(aBoolean)));

        //Запрос на закрытие отображаемой новости
        mCompositDisposable.add(mNewsView.getDismissDlgSubject()
            .subscribe(aBoolean->mNewsModel.closeNews()));

        //Отображение ошибки
        mCompositDisposable.add(mNewsModel.getErrorCodeSubject()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(aString -> {
                mNewsView.showMessage(aString);
                //Скрываем диалог загрузки
                mNewsView.showProgress(false);
            }));

        //отписка по destroy
        mCompositDisposable.add(mNewsView.getUnBindViewSubject()
                .subscribe(aBoolean -> unBindView()));
    }

    @Override
    public void unBindView() {

        mNewsView = null;
        if(mCompositDisposable!=null)
            mCompositDisposable.dispose();

        mListPresenter.unBind();
    }

}

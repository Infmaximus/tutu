package com.infmaximus.tututest.view.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.infmaximus.tututest.R;
import com.infmaximus.tututest.objects.News;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by InfMaximus on 18.11.2017.
 */

public class AdapterListNews extends RecyclerView.Adapter<AdapterListNews.ViewHolder> implements BaseAdapterList{
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm yyyy-MM-dd");
    private List<News> arrayList = new ArrayList<>();
    //Сабджект для информирования о том, какую статью следует отобразить
    private PublishSubject<Integer> mUriSubject = PublishSubject.create();
    public AdapterListNews(){
    }

    @Override
    public void setArrayList(List<News> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_title_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvTitle.setText(arrayList.get(position).getShortText());
        holder.tvTitle.setOnClickListener(o-> mUriSubject.onNext(position));
        holder.tvDate.setText(arrayList.get(position).getDate());

       /* //Преобразование изображения в нужный размер
        Transformation transformation = new Transformation() {
            @Override
            public Bitmap transform(Bitmap bitmap) {
                int width = holder.ivNewsImage.getWidth();

                double relation = (double) bitmap.getHeight() / (double) bitmap.getWidth();
                int targetHeight = (int) (width * relation);
                Bitmap result = Bitmap.createScaledBitmap(bitmap, width, targetHeight, false);
                if (result != bitmap) {
                    bitmap.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "key";
            }
        };*/

        //Получаем строку для формата URI
        String urlStr = arrayList.get(position).getImageUri();
        String url = Uri.parse(urlStr)
                .buildUpon()
                .build()
                .toString();

/*        Picasso.Builder builder = new Picasso.Builder(mContext)
                .memoryCache(new LruCache(4000));
        builder.listener((picasso, uri, exception) -> {
            exception.printStackTrace();
            Log.d("LOG_TAG","ошибка"+exception.toString());
        });

        //Производим загрузку изображений
        builder.build()
                .load(url)
                .transform(transformation)
                .placeholder(mContext.getResources().getDrawable(R.drawable.tnyt_logo))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.ivNewsImage, new Callback() {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError() {
                        //Если загрузка из кеша не удалась, грузим по сети
                        Picasso.with(mContext)
                                .load(url)
                                .transform(transformation)
                                .error(mContext.getResources().getDrawable(R.drawable.tnyt_logo))
                                .into(holder.ivNewsImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }
                                    @Override
                                    public void onError() {
                                        Log.v("Picasso","Could not fetch image");
                                    }
                                });
                    }
                });*/

        holder.frescoView.setImageURI(url);
        holder.frescoView.getHierarchy().setPlaceholderImage(R.drawable.tnyt_logo);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDate;
        //ImageView ivNewsImage;
        SimpleDraweeView frescoView;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView)itemView.findViewById(R.id.tvTitle);
            //ivNewsImage = (ImageView)itemView.findViewById(R.id.ivNewsImage);
            tvDate = (TextView)itemView.findViewById(R.id.tvDateNew);
            frescoView = (SimpleDraweeView) itemView.findViewById(R.id.frescoView);
        }
    }

    @Override
    public PublishSubject<Integer> getUriSubject() {
        return mUriSubject;
    }
}

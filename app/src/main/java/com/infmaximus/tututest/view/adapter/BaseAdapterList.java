package com.infmaximus.tututest.view.adapter;

import com.infmaximus.tututest.objects.News;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by InfMaximus on 18.11.2017.
 */

public interface BaseAdapterList {

    //void setArrayList(ArrayList<News> arrayList);
    void setArrayList(List<News> arrayList);
    void notifyDataSetChanged();
    PublishSubject<Integer> getUriSubject();
}

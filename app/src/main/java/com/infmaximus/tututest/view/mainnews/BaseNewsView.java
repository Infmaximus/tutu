package com.infmaximus.tututest.view.mainnews;

import com.infmaximus.tututest.view.adapter.BaseAdapterList;

import io.reactivex.Flowable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

/**
 * Интерфейс главного экрана
 */

public interface BaseNewsView {

    //Отобразить прогрессбар
    void showProgress(boolean aStateDialog);
    //Отобразить сообщение
    void showMessage(String aString);

    //Необходимо инициировать элемент отображения новостей
    void initializationRecycler();
    //Показать скрыть диалог, содержащий новость
    void initializationDlg(String aUriString);

    //Источник получения инициированного адаптера
    BehaviorSubject<BaseAdapterList> getAdapter();
    //Источник информирования нажатия на кнопку
    Flowable<Boolean> getPSBtnUpdateClick();
    //Источники нформирования прокрутки вниз списка (для загрузки новой порции новостей)
    Flowable<Boolean> getRecyclerScrollEnd();
    //Источник информации о смерти активити
    PublishSubject<Boolean> getUnBindViewSubject();
    //Источник информации о закрытии отображаемой новости
    PublishSubject<Boolean> getDismissDlgSubject();

}

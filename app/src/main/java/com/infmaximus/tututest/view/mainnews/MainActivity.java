package com.infmaximus.tututest.view.mainnews;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.infmaximus.tututest.R;
import com.infmaximus.tututest.view.adapter.AdapterListNews;
import com.infmaximus.tututest.view.adapter.BaseAdapterList;
import com.infmaximus.tututest.app.App;
import com.infmaximus.tututest.presenter.mainnews.BaseNewsPresenter;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity implements BaseNewsView {

    @Inject
    BaseNewsPresenter mNewsPresenter;

    private FloatingActionButton fab;
    private RecyclerView rvTitleList;
    private ProgressBar pbLoadNews;
    private Dialog mDialog;
    //Информация о разрушении Activity
    private PublishSubject<Boolean> mUnBindViewSubject = PublishSubject.create();
    private PublishSubject<Boolean> mDismissDlgSubject = PublishSubject.create();
    private BehaviorSubject<BaseAdapterList> mProvideRecyclerSubject = BehaviorSubject.create();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        App.getAppComponent().inject(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        rvTitleList = (RecyclerView) findViewById(R.id.rvTitleList);
        pbLoadNews = (ProgressBar) findViewById(R.id.pbLoadNews);
        initializationRecycler();
        mNewsPresenter.bindView(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnBindViewSubject.onNext(true);
    }

    @Override
    public void showProgress(boolean aStateDialog) {
        pbLoadNews.setVisibility(aStateDialog?View.VISIBLE:View.GONE);
    }

    @Override
    public void showMessage(String aTextMessage) {
        Toast.makeText(this,aTextMessage,Toast.LENGTH_LONG).show();

    }

    @Override
    public void initializationRecycler() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        AdapterListNews mAdapterListNews = new AdapterListNews();
        rvTitleList.setLayoutManager(mLinearLayoutManager);
        rvTitleList.setAdapter(mAdapterListNews);
        mProvideRecyclerSubject.onNext(mAdapterListNews);
    }

    //Инициализация диалога отображения статьи либо скрытие текущего диалога по пустой строке
    @Override
    public void initializationDlg(String aUriString) {
        if(TextUtils.isEmpty(aUriString)){
            if(mDialog!=null)
                mDialog.dismiss();
        }else {
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.dialog_web_view_layout, null);
            WebView webView = (WebView) dialogLayout.findViewById(R.id.webViewNews);
            webView.setWebViewClient(new WebViewClient());
            Button btnDismiss = (Button) dialogLayout.findViewById(R.id.btnDismiss);
            btnDismiss.setOnClickListener(view -> mDismissDlgSubject.onNext(true));
            Uri data = Uri.parse(aUriString);
            webView.loadUrl(data.toString());
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setView(dialogLayout);
            mDialog = builder.create();
            mDialog.show();
        }
    }

    @Override
    public BehaviorSubject<BaseAdapterList> getAdapter() {
        return mProvideRecyclerSubject;
    }

    public PublishSubject<Boolean> getUnBindViewSubject() {
        return mUnBindViewSubject;
    }

    @Override
    public PublishSubject<Boolean> getDismissDlgSubject() {
        return mDismissDlgSubject;
    }

    //Источник информации об окончании прокрутки
    @Override
    public Flowable<Boolean> getRecyclerScrollEnd() {
        return Flowable.create(
                e1 -> rvTitleList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (!recyclerView.canScrollVertically(1)) {
                            e1.onNext(true);
                        }
                    }
                }), BackpressureStrategy.BUFFER);
    }

    //Обработчик нажатия на кнопку обновления новостей
    @Override
    public Flowable<Boolean> getPSBtnUpdateClick() {
            return Flowable.create(
                    e1 -> fab.setOnClickListener(
                            view -> e1.onNext(true)), BackpressureStrategy.BUFFER);
    }
}

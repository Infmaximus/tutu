package com.infmaximus.tututest;

import android.content.Context;
import android.net.ConnectivityManager;

import com.infmaximus.tututest.model.NewsModel;
import com.infmaximus.tututest.model.newsapi.BaseNews;
import com.infmaximus.tututest.model.newsapi.NYTimesNewsApi;
import com.infmaximus.tututest.objects.News;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricGradleTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by InfMaximus on 20.11.2017.
 */

//@RunWith(RobolectricGradleTestRunner.class)
public class ModelTest {

    @Mock
    NYTimesNewsApi fakeNytNewsApi;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    Context faceContext;

    @Mock
    ConnectivityManager fakeCM;

    NewsModel mNewsModel;

/*    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mNewsModel = new NewsModel(fakeNytNewsApi);
    }*/

    @Test
    public void verifyNewsList(){
        mNewsModel = new NewsModel(fakeNytNewsApi);
        /*List<News> arrayList = new ArrayList<>();
        arrayList.add(new News.Builder().setShortText("Hello").build());

        List<BaseNews> baseNews = new ArrayList<>();
        baseNews.add(new BaseNews() {
            @Override
            public String getShortNews() {
                return "hello";
            }

            @Override
            public String getImageUri() {
                return null;
            }

            @Override
            public String getWebSite() {
                return null;
            }

            @Override
            public String getTime() {
                return null;
            }

            @Override
            public String getLargeNews() {
                return null;
            }
        });

        when(fakeNytNewsApi.convertDate("2017-11-19T21:03:28+0000")).thenReturn("2017-11-19 21:03:28");

        PublishSubject<News> behaviorSubject = PublishSubject.create();
        behaviorSubject.onNext(arrayList.get(1));
        when(fakeNytNewsApi.getListNewsListSubject()).thenReturn(behaviorSubject)
        when(fakeNytNewsApi.getListNewsListSubject()).thenReturn(mNewsModel.getLastNewsListSubject())

        mNewsModel.openNews(1);
        Mockito.verify(fakeNytNewsApi).convertInfoToNews(arrayList.get(1));

        when(fakeNytNewsApi.getListNewsListSubject()).thenReturn(mNewsModel.getLastNewsListSubject())*/

        when(faceContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(fakeCM);
        mNewsModel.isOnline();
        verify(fakeCM).getActiveNetworkInfo();

    }

}
